'use strict';

const fs = require('fs').promises;
const path = require('path');

class PackageReader {
    constructor() {}


    async getDependencies(filePath = './repo-package.json') {
        try {
            let packagePath = path.resolve(filePath);
            console.log(packagePath)
            const file = await this._readPackage(packagePath); 
            return this._getAllDependencies(file);
        } catch (err) {
            throw err;
        }
    }

    async _readPackage(packagePath) {
        return await fs.readFile(packagePath)
            .catch((err) => {
                throw err;
            });
    }

    _getAllDependencies(file) {
        const jsonFile = JSON.parse(file.toString());

        return {
            dependencies: {
                ...jsonFile.dependencies,
            },
            devDependencies: {
                ...jsonFile.devDependencies,
            },
        };
    }
}

module.exports = PackageReader;