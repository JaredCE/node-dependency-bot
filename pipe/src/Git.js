'use strict';

const childProcess = require('child_process');

class Git {
    constructor() {

    }

    async gitBranch() {
        let cmd = 'git checkout -b node-depend-bot-update';
        await this._runGitCommand(cmd)
            .catch((err) => {
                throw err;
            });
    }

    async gitAdd() {
        let cmd = 'git add package.json package-lock.json';
        await this._runGitCommand(cmd)
            .catch((err) => {
                throw err;
            });
    }

    async gitCommit(message) {
        await this.gitAdd()
            .catch((err) => {
                throw err;
            });
        let cmd = `git commit -m "${message}"`;
        await this._runGitCommand(cmd)
            .catch((err) => {
                throw err;
            });
    }

    async gitPR() {
        let cmd = 'git push -u origin node-depend-bot-update';
        await this._runGitCommand(cmd)
            .catch((err) => {
                throw err;
            });
    }

    _runGitCommand(cmd) {
        return new Promise((resolve, reject) => {
            const commandRun = childProcess.exec(cmd);

            
        });
    }
}

module.exports = Git;