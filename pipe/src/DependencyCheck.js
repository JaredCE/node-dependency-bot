'use strict';

const https = require('https');

class DependencyChecker {
    constructor() {
        this.registry = 'registry.npmjs.org';
    }

    async getLatestDependencies(dependencies) {
        const newDependencies = {
            dependencies: {},
            devDependencies: {},
        };

        newDependencies.dependencies = await this._getLatestDependencies(dependencies.dependencies);
        newDependencies.devDependencies = await this._getLatestDependencies(dependencies.devDependencies)

        return newDependencies;
    }

    async _getLatestDependencies(dependencies) {
        const obj = {};
        for (const dependency in dependencies) {
            const latestDependency = await this._getDependency(dependency)
                .catch((err) => {
                    return {};
                });
            
            const updatedDependency = this._dependencyNeedsUpdate(
                latestDependency, 
                dependencies[dependency],
                {
                    dependencyName: dependency,
                    version: dependencies[dependency]
                }
            );

            if (Object.keys(updatedDependency).length > 0) {
                Object.assign(obj, updatedDependency);
            }
        }

        return obj;
    }

    _dependencyNeedsUpdate(dependencyInformation, currentVersion, dependency) {
        const latestVersion = dependencyInformation?.['dist-tags']?.latest;
        let updatedDependency = {};
        if (latestVersion !== undefined) {
            const version = currentVersion.replace('^', '');
            if (version !== latestVersion) {
                updatedDependency = {
                    [dependency['dependencyName']]: latestVersion
                }
            }
        }
        return updatedDependency;
    }



    async _getDependency(dependency) {
        const options = {
            hostname: this.registry,
            path: `/${dependency}/`
        };

        return new Promise((resolve, reject) => {
            https.get(options, (res) => {
                const { statusCode } = res;
                const contentType = res.headers['content-type'];
              
                let error;
                // Any 2xx status code signals a successful response but
                // here we're only checking for 200.
                if (statusCode !== 200) {
                  error = new Error('Request Failed.\n' +
                                    `Status Code: ${statusCode}`);
                } else if (!/^application\/json/.test(contentType)) {
                  error = new Error('Invalid content-type.\n' +
                                    `Expected application/json but received ${contentType}`);
                }

                if (error) {
                  // Consume response data to free up memory
                  res.resume();
                  reject(error);
                }
              
                res.setEncoding('utf8');
                let rawData = '';
                res.on('data', (chunk) => { rawData += chunk; });
                res.on('end', () => {
                  try {
                    const parsedData = JSON.parse(rawData);
                    resolve(parsedData);
                  } catch (e) {
                    reject(e)
                  }
                }); 
            }).on('error', (e) => {
                reject(e);
            });
        });
    }
}

module.exports = DependencyChecker;