'use strict';



class NPMInstall {
    constructor(git) {
        this.git = git;
    }

    installNewDependencies(dependencies) {
        this.git.gitBranch();
        const depsUpdated = this._runDependencyInstall(dependencies.dependencies);
        if (depsUpdated) this.git.gitCommit('updated dependencies');
        const devDepsUpdated = this._runDependencyInstall(dependencies.devDependencies, true);
        if (devDepsUpdated) this.git.gitCommit('updated development dependencies');
        this.git.gitPR();
    }

    _runDependencyInstall(dependencies, development= false) {
        let updated = false;
        if (Object.keys(dependencies).length > 0) {
            let str = 'npm i';
            if (development) {
                str += ' --save-dev'
            }

            for (const dependency in dependencies) {
                str += ` ${dependency}@${dependencies[dependency]}`
            }

            console.log(str);
            updated = true;
        }

        return updated;
    }
}

module.exports = NPMInstall