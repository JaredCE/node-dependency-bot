'use strict';

const PackageReader = require('./src/PackageReader');
const DependencyChecker = require('./src/DependencyCheck');
const NPMInstall = require('./src/NPMInstall');
const Git = require('./src/Git');

const main = async () => {
    const packageReader = new PackageReader();
    const dependencyCheck = new DependencyChecker();
    const git = new Git();
    const npmInstaller = new NPMInstall(git);

    try {
        const packages = await packageReader.getDependencies('./package.json');
        const latestVersions = await dependencyCheck.getLatestDependencies(packages);
        if (Object.keys(latestVersions.dependencies).length > 0 ||
            Object.keys(latestVersions.devDependencies).length > 0) {
            npmInstaller.installNewDependencies(latestVersions);
        }
    } catch(err) {
        console.log(err)
    }
}

main()
    .catch((err) => {
        throw err;
    });