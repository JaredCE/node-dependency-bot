FROM node:lts-alpine3.13

RUN apk add git 

COPY pipe /

ENTRYPOINT [ "node", "index.js" ]